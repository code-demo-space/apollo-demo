package com.apollo.demo.service;

import java.util.Properties;
import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;

/**
 * Apollo配置中心使用测试服务
 *
 * @author chentiefeng
 * @date 2021/4/22 16:27
 */
public class ApolloTestService {
    public static void main(String[] args) {
        // 默认值是application
        String namespace = "application";
        //initProperties();
        String apolloMetaServer = "http://127.0.0.1:8080";
        String appId = "SampleApp";
        System.setProperty("app.id", appId);
        System.setProperty("apollo.meta", apolloMetaServer);
        Config config = ConfigService.getAppConfig();
        String someKey = "timeout";
        String someDefaultValue = "3000";
        String value = config.getProperty(someKey, someDefaultValue);
        System.out.println(value);
    }

    public static void initProperties() {
        String appId = "SampleApp";
        String apolloMetaServer = "http://localhost:8070";
        Properties properties = new Properties();

        // AppId是应用的身份信息，是从服务端获取配置的一个重要信息
        properties.setProperty("app.id", appId);

        // 使用Apollo的Meta Server机制来实现Config Service的服务发现
        properties.setProperty("apollo.meta", apolloMetaServer);

        // 直接指定Config Service地址的方式来跳过Meta Server服务发现
        // properties.setProperty("apollo.configService", apolloMetaServer);

        // 支持以下方式自定义缓存路径, 默认是/opt/data或C:\opt\data\目录
        properties.setProperty("apollo.cacheDir", "C:\\opt\\data");

        // Environment配置支持以下几个值（大小写不敏感）DEV、FAT、UAT、PRO
        properties.setProperty("env", "DEV");

        // Apollo支持配置按照集群划分，也就是说对于一个appId和一个环境，对不同的集群可以有不同的配置。
        properties.setProperty("apollo.cluster", "default");

        // 设置内存中的配置项是否保持和页面上的顺序一致
        properties.setProperty("apollo.property.order.enable", "true");

        // 访问密钥机制，从而只有经过身份验证的客户端才能访问敏感配置。如果应用开启了访问密钥，客户端需要配置密钥，否则无法获取配置。
        // properties.setProperty("apollo.accesskey.secret", "1cf998c4e2ad4704b45a98a509d15719");

        //自定义server.properties路径
        // properties.setProperty("apollo.path.server.properties", "/some-dir/some-file.properties");

        System.setProperties(properties);
    }
}
